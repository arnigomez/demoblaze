package co.com.demoblaze.certificacion.demoblaze.stepdefinitions;

import co.com.demoblaze.certificacion.demoblaze.questions.ValidateLaptopDetails;
import co.com.demoblaze.certificacion.demoblaze.questions.ValidateSesion;
import co.com.demoblaze.certificacion.demoblaze.task.Login;
import co.com.demoblaze.certificacion.demoblaze.task.ProductSection;
import co.com.demoblaze.certificacion.demoblaze.utils.Driver;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;

public class DemoblazeStepDefinitions {


    @Managed(uniqueSession = true)
    private WebDriver driver;

    @Before
    public void init() {
        setTheStage(Cast.ofStandardActors());
        theActorCalled("Juan");
    }


    @Dado("que un cliente quiere iniciar sesion en demoblaze")
    public void queUnClienteQuiereIniciarSesionEnDemoblaze() {
        theActorInTheSpotlight().can(BrowseTheWeb.with(Driver.demoblazeDriver(driver)));
    }

    @Cuando("el escribe  {string} y {string}")
    public void elEscribeUsuarioYContrasena(String  usuario,String contrasena) {
        theActorInTheSpotlight().attemptsTo(Login.enDemoblaze(usuario,contrasena));
    }

    @Entonces("valida que la sesion  haya sido iniciada")
    public void validaQueLaSesionHayaSidoIniciada() {
        theActorInTheSpotlight().should(seeThat(ValidateSesion.validasesion()));
    }

    @Dado("que un cliente quiere iniciar sesion en demoblaze con {string} y {string}")
    public void queUnClienteQuiereIniciarSesionEnDemoblazeConY(String usuario, String contrasena) {
        theActorInTheSpotlight().can(BrowseTheWeb.with(Driver.demoblazeDriver(driver)));
        theActorInTheSpotlight().attemptsTo(Login.enDemoblaze(usuario,contrasena));
    }

    @Cuando("el selecciona la seccion")
    public void elSeleccionaLaSeccion() {
        theActorInTheSpotlight().attemptsTo(ProductSection.enLaptops());
    }


    @Entonces("el puede ver los dealles de la referencia")
    public void elPuedeVerLosDeallesDeLaReferencia() {
        theActorInTheSpotlight().should(seeThat(ValidateLaptopDetails.validadetallesdelaptop()));
    }
    @After
    public void cerrar() {
        BrowseTheWeb.as(theActorInTheSpotlight()).getDriver().quit();
    }

}

