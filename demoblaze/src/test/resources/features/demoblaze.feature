# language: es

Característica:  Navegar y visualizar los productos
  de tipo Laptopo con sus detalles
  Quiero ingresar a la pagina www.demoblaze.com
  para realizar decidir la compra de un articulo

yreturn Tasks.instrumented(ProductSection.class);

  Esquema del escenario: : iniciar sesion en demoblaze
    Dado que un cliente quiere iniciar sesion en demoblaze
    Cuando  el escribe  "<usuario>" y "<contrasena>"
    Entonces valida que la sesion  haya sido iniciada
    Ejemplos:
      | usuario | contrasena      |
      | seya    | G@HgUpkiup39FJR |


  Esquema del escenario: : navegar hasta la seccion de productos Laptops
    Dado que un cliente quiere iniciar sesion en demoblaze con "<usuario>" y "<contrasena>"
    Cuando el selecciona la seccion
    Entonces el puede ver los dealles de la referencia
    Ejemplos:
      | usuario | contrasena      |
      | seya    | G@HgUpkiup39FJR |


