package co.com.demoblaze.certificacion.demoblaze.utils;

import net.thucydides.core.webdriver.SerenityWebdriverManager;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Driver {

    public static WebDriver demoblazeDriver(WebDriver driver) {
        driver.manage().window().maximize();
        driver.get("https://www.demoblaze.com/?language=en_US");
        SerenityWebdriverManager.inThisTestThread().setCurrentDriverTo(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return driver;
    }
}
