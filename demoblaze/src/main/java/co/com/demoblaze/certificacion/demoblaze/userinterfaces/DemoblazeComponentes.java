package co.com.demoblaze.certificacion.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class DemoblazeComponentes {

    public static final Target LOGIN = Target.the("login").locatedBy("#login2");
    public static final Target USERNAME = Target.the("username").locatedBy("#loginusername");
    public static final Target PASSWORD = Target.the("password").locatedBy("#loginpassword");
    public static final Target LOGIN2 = Target.the("login2").locatedBy("[onclick='logIn()']");
    public static final Target LAPTOPS = Target.the("laptops").locatedBy("//a[text()='Laptops']");
    public static final Target SONYVAIOI5 = Target.the("sonyvaioi5").locatedBy("//a[text()='Sony vaio i5']");
    public static final Target WELCOME = Target.the("welcome").locatedBy("#nameofuser");

    private DemoblazeComponentes() {}

}


