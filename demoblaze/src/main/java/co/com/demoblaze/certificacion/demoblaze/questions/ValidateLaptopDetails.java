package co.com.demoblaze.certificacion.demoblaze.questions;

import co.com.demoblaze.certificacion.demoblaze.userinterfaces.DemoblazeComponentes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.time.Duration;

public class ValidateLaptopDetails implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {

        return DemoblazeComponentes.SONYVAIOI5.waitingForNoMoreThan(Duration.ofSeconds(10))
                .resolveFor(actor).getText().equals("Sony vaio i5");
    }

    public static ValidateLaptopDetails validadetallesdelaptop() {
        return new ValidateLaptopDetails(); }
}