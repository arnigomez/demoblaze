package co.com.demoblaze.certificacion.demoblaze.task;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.time.Duration;

import static co.com.demoblaze.certificacion.demoblaze.userinterfaces.DemoblazeComponentes.*;


public class Login implements Task {

    private String usuario;
    private String contrasena;

    public Login(String usuario,String contrasena) {
        this.usuario = usuario;
        this.contrasena = contrasena;

    }

    @Override
    public <T extends Actor> void performAs(T actor) {
            actor.attemptsTo(
                    Click.on(LOGIN),
                    Enter.theValue(usuario).into(USERNAME.waitingForNoMoreThan(Duration.ofSeconds(10))),
                    Enter.theValue(contrasena).into(PASSWORD.waitingForNoMoreThan(Duration.ofSeconds(10))),
                    Click.on(LOGIN2.waitingForNoMoreThan(Duration.ofSeconds(10)))
            );
    }

    public static Login enDemoblaze(String usuario,String contrasena) {
        return Tasks.instrumented( Login.class, usuario,contrasena);
    }
}
