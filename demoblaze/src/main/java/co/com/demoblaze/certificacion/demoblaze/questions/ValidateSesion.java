package co.com.demoblaze.certificacion.demoblaze.questions;

import co.com.demoblaze.certificacion.demoblaze.userinterfaces.DemoblazeComponentes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;


import java.time.Duration;

public class ValidateSesion implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {

        return DemoblazeComponentes.WELCOME.waitingForNoMoreThan(Duration.ofSeconds(10)).resolveFor(actor).getText().contains("Welcome");
    }

    public static ValidateSesion validasesion() {
        return new ValidateSesion();
}
}