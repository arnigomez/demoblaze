package co.com.demoblaze.certificacion.demoblaze.task;
import co.com.demoblaze.certificacion.demoblaze.userinterfaces.DemoblazeComponentes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.JavaScriptClick;


import java.time.Duration;


public class ProductSection implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                JavaScriptClick.on(DemoblazeComponentes.LAPTOPS.waitingForNoMoreThan(Duration.ofSeconds(10))),
                JavaScriptClick.on(DemoblazeComponentes.SONYVAIOI5.waitingForNoMoreThan(Duration.ofSeconds(10))));
    }

    public static ProductSection enLaptops() {

        return Tasks.instrumented(ProductSection.class);
    }
}
